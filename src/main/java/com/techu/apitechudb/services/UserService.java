package com.techu.apitechudb.services;

import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public List<UserModel> findAll(String orderBy){

        System.out.println("findAll en UserService");

        List<UserModel> result;

        if(orderBy != null){
            System.out.println("Se ha podido ordenar");
            result = this.userRepository.findAll(Sort.by("age"));
        }  else{
            result = this.userRepository.findAll();
        }

        return result;
    }

    public Optional<UserModel> findById(String id){
        System.out.println("findById en UserService");

        return this.userRepository.findById(id);
    }

    public UserModel add(UserModel user){

        System.out.println("add en UserService");
        return this.userRepository.save(user);
    }

    public UserModel update(UserModel user) {
        System.out.println("Update en UserService");

        return this.userRepository.save(user);
    }

    public boolean delete(String id) {
        System.out.println("delete en UserService");

        boolean result = false;

        if(this.findById(id).isPresent() == true){
            System.out.println("Usuario encontrado, borrando...");

            this.userRepository.deleteById(id);
            result = true;
        }

        return result;
    }
}

