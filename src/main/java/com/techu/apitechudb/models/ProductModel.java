package com.techu.apitechudb.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

//ORM

//UPSERT -> viene por usar la funcion save de mongo, si se usase INSERT entonces al
//insertar dos veces daría alguna clase de error, duplicado o el que fuese.

@Document(collection = "products")
public class ProductModel {

    @Id
    private String id;
    private String desc;
    private float price;

    public ProductModel() {
    }

    public ProductModel(String id, String desc, float price) {
        this.id = id;
        this.desc = desc;
        this.price = price;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDesc() {
        return this.desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public float getPrice() {
        return this.price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
}
